//Filename: Employee.java
//Usage: This is an absttract class for Employee
//Author: Nyah Check

public abstract class  Employee {

    private String firstName;
    private String lastName;
    private String SSN;

    public Employee(String firstName, String lastName, String SSN) {

	setFirstName(firstName);
	setLastName(lastName);
	setSSN(SSN);

    }

    //mutators
    public void setFirstName(String firstName) {

        this.firstName = firstName;
    }

    public void setLastName(String lastName) {

	this.lastName = lastName;
    }

    public void setSSN(String SSN) {
	this.SSN = SSN;

    }

    //accessors
    public String getFirstName() {
	return firstName;

    }

    public String getLastName() {

	return lastName;
    }

    public String getSSN() {

	return SSN;
    }

    //abstract method with no implementation yet.
    public abstract double earnings();

    /**
     * This overrides the toString method for all objects.
     */
    @Override
    public String toString() {

	return String.format("\nSocial Security Number: %s\n, First Name: %s, Last Name: %s\n", getSSN(), getFirstName(), getLastName());

    }
}
    
