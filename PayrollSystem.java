//filename: PayrollSystem.java
//Usage: This tests polymorphic behaviour by invoking and developming a payroll system. for employees. 
//Author: Nyah Check

import java.util.*;


public class PayrollSystem {

    public static void main(String[] args) {

	Employee[] workers = new Employee[4];

	SalariedEmployee salaried = new SalariedEmployee("Ako", "Takum", "024-184-382", 5000);
	HourlyEmployee hourly = new HourlyEmployee("Sandra", "Menka", "193-382-832", 2000, 8);

	CommissionEmployee commissioned = new CommissionEmployee("Tobias", "Sama", "204-382-839", 14500, 0.20);

	BasePlusCommissionEmployee baseCommissioned = new BasePlusCommissionEmployee("Frederick", "Gerhardt", "2482-284-282", 492,.24, 5000);

	
	//processing the earnings of employees individually
	System.out.printf("\nEmployees Data Processed Individually\n\n");

	System.out.printf("\nEmployee 1: %s, Earnings: %.3f",salaried.toString(), salaried.earnings());

	System.out.printf("\nEmployee 2: %s, Earnings: %.3f", hourly.toString(), hourly.earnings());

	System.out.printf("\nEmployee 3: %s, Earnings: %.3f", commissioned.toString(), commissioned.earnings());

	System.out.printf("\nEmployee 4: %s, Earnings: %.3f", baseCommissioned.toString(), baseCommissioned.earnings());


	//processing Employees Polymorphically.
	workers[0] = salaried;
	workers[1] = hourly;
	workers[2] = commissioned;
	workers[3] = baseCommissioned;
	System.out.printf("\n------------------------------------\nProcessing Polymorphically.\n\n");

	for( Employee obj: workers) {
	    System.out.printf("\n Employee: %s, Earnings: %2.f\n", obj.toString(), obj.earnings());

	}//end for

    }//end main method.

}//end Payroll class.

	
