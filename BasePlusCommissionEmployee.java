//Filename: BasePlusCommissionEmployee.java
//Usage: Class creates a commission employee with a base salary
//Author: Nyah Check


public class BasePlusCommissionEmployee extends CommissionEmployee {

    private double baseSalary;

    public BasePlusCommissionEmployee( String firstName, String lastName, String SSN, int grossSales, double commissionRate, double baseSalary) {

	super(firstName, lastName, SSN, grossSales, commissionRate);
	
	setBaseSalary(baseSalary);

    }

    //mutators.
    public void setBaseSalary(double baseSalary) {

	if( baseSalary >= 0 ) 
	    this.baseSalary = baseSalary;
	else 
	    throw new IllegalArgumentException("Invalid Base Salary");

    }

    //accessors.

    public double getBaseSalary() {
	
	return baseSalary;
    }

    @Override
    public double earnings() {
	return (super.earnings() + getBaseSalary());

    }


    @Override
    public String toString() {

	return String.format("%s\n Base Salary: %.3f",super.toString(), getBaseSalary());

    }

}//end of class.
	    
