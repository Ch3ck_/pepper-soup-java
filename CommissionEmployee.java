//Filename: CommissionEmployee.java
//Usage: This is a commission employee who makes earning based on his commisions per sale
//Author: Nyah Check

public class CommissionEmployee extends Employee {

    private int grossSales;
    private double commissionRate;

    public CommissionEmployee(String firstName, String lastName, String SSN, int grossSales, double commissionRate) {

	super(firstName, lastName, SSN);

	setGrossSales(grossSales);
	setCommissionRate(commissionRate);

    }

    public void setGrossSales( int grossSales) {

	if (grossSales >= 0) 
	    this.grossSales = grossSales;
	else 
	    throw new IllegalArgumentException("Invalid gross Sales");
    }

    public void setCommissionRate(double commissionRate) {
	
	if (commissionRate >= 0 && commissionRate < 1) 
	    this.commissionRate = commissionRate;
	else
	    throw new IllegalArgumentException("Invalid commission Rates");

	
    }

    public int getGrossSales() {
	
	return grossSales;
    }

    public double getCommissionRate() {

	return commissionRate;

    }

    @Override
    public double earnings() {

	return (getCommissionRate() * getGrossSales());
    }

    @Override
    public String toString() {

	return String.format("%s\n, Commission Rates: %.3f, Gross Sales: %d\n", super.toString(), getCommissionRate(), getGrossSales());

    }
}
