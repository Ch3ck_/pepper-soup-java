/**
 *Filename: ArrayManipulations.java
 *Usage: This makes use of the Arrays class to manipulate arrays.
 *Author: Nyah Check
 */

import java.util.Arrays;

public class ArrayManipulations {

	public static void main(String[] args) {

		double[] Array = {39.4, 29.4, 1.4, 9.4, 29.4, 1,3 , 9, 1, 45, 32.0, 4.23};

		//this sorts the Array in Ascending order.
		Arrays.sort(Array);
		
		//prints the sorted array to screen.
		displayArray(Array, "Original Array");

		double[] newA = new double[Array.length];

		//copies the data from Array to newA
		System.arraycopy(Array, 0, newA, 0, newA.length);
		
		//prints the data on the newly copied array.
		displayArray(newA, "Copied Array");

		//Creates a new array and fills it with 8.23
		double[] A = new double[newA.length];

		Arrays.fill(A, 8.23);
		displayArray(A, "Filled Array");

		//compares two arrays for equality
		Boolean b = Arrays.equals(Array, newA);
		System.out.printf("\nArrays %s to New Array", ((!b) ? "is not equal" : "is equal"));

		//searches for a number in an array.
		int location = Arrays.binarySearch(Array, 20);
		if (location >=  0)
			System.out.printf("\n 20 is located at position: %d", location);

		else 
			System.out.printf("\n 20 is not found in the Array.");
	}

	public static void displayArray(double[] A, String t) {
		
		System.out.println();
		System.out.println(t);
		for( double n: A) 
			System.out.printf("\n %.2f", n);

       }
}
