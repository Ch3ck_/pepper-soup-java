/*
 * This program initializes an array using command line arguments.
 * Filename: Gradebook.java
 */
import java.util.*;

/**
 *
 * @author localhost
 */
public class Gradebook {

    /**
     * This program reads data from the command line and processes this data on 
     *  an array.
     * @param args
     */
    public static void main(String[] args) {
        
        int[] Array = new int [args.length];
        //reads the array data from the command line and stores them to a file.
        int i = 0;
        for( String str: args) {
            Array[i++] = Integer.parseInt(str);
        }
        
        //prints the values on the array to the screen.
        for(int num: Array) {
            System.out.printf("\n %d", num);
        }
    }
}

