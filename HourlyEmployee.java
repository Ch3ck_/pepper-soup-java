//Filename: HourlyEmployee.java
//Usage: This is a salaried employee which extends the Employee class.
//Author: Nyah Check


public class HourlyEmployee extends Employee {

    private double wage;
    private int hours;

    public HourlyEmployee(String firstName, String lastName, String SSN, double wage, int hours) {

	super(firstName, lastName, SSN);
	setHours(hours);
	setWage(wage);
    }


    //mutators.
    public void setWage(double wage) {
	
	if (wage >= 0) 
	    this.wage = wage;
	else
	    throw new IllegalArgumentException("Invalid Wage. Please try again.");

    }

    public void setHours(int hours) {

	if (hours>= 0) 
	    this.hours = hours;

	else 
	    throw new IllegalArgumentException("Invalid work Hours.");

    }

    public double getWage() {
	
	return wage;
    }

    public int getHours() {
	
	return hours;
    }

    @Override
    public double earnings() {
	
	return (getHours() * getWage());

    }

    @Override
    public String toString() {

	return String.format(" %s\n Hours Worked: %d, Hourly Wage: %.3f", super.toString(), getHours(), getWage());

    }
}
