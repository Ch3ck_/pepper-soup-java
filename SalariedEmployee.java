//Filename: SalariedEmployee.java
//Usage: This is a salaried employee which extends the Employee class.
//Author: Nyah Check


public class SalariedEmployee extends Employee {

    private double weeklySalary;
    

    public SalariedEmployee(String firstName, String lastName, String SSN, double weeklySalary) {

	super(firstName, lastName, SSN);
	setWeeklySalary(weeklySalary);
    }


    //mutators.
    public void setWeeklySalary(double weeklySalary) {
	
	if (weeklySalary >= 0) 
	    this.weeklySalary = weeklySalary;
	else
	    throw new IllegalArgumentException("Invalid Weekly Salary.");

    }

    public double getWeeklySalary() {
	
	return weeklySalary;
    }


    @Override
    public double earnings() {
	
	return (getWeeklySalary() * 4);

    }

    @Override
    public String toString() {

	return String.format(" %s\n Weekly Salary: %.3f", super.toString(), getWeeklySalary());

    }//end toString.

}//end class
